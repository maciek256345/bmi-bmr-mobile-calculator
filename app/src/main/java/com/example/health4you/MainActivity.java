package com.example.health4you;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;



public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_screen);
    }

    public void BMIButtonClicked(View view) {
        Intent bmi = new Intent(this, BMI.class);
        startActivity(bmi);

    }


    public void BMRButtonClicked(View view) {
        Intent bmr = new Intent(this, BMR.class);
        startActivity(bmr);
    }












}
package com.example.health4you;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

public class BMR extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bmr);
    }



    public void BMRCalculateButtonClicked(View view) {
        EditText edtMass = (EditText) findViewById(R.id.edtMass2);
        EditText edtHeight = (EditText) findViewById(R.id.edtHeight2);
        EditText edtAge = (EditText) findViewById(R.id.edtAge);
        TextView BMRresult = (TextView) findViewById(R.id.tvBMRResult);
        TextView yourBMR = (TextView) findViewById(R.id.tvYourBMR);

        RadioButton women = (RadioButton) findViewById(R.id.radioWomen);
        RadioGroup group = (RadioGroup) findViewById(R.id.radioGroup);

        double mass = Double.parseDouble(edtMass.getText().toString());
        double height = Double.parseDouble(edtHeight.getText().toString());
        double age = Double.parseDouble(edtAge.getText().toString());

        double BMR;


        if (group.getCheckedRadioButtonId() == women.getId()) {
            BMR = (9.99 * mass) + (6.25 * height) - (4.92 * age) - 161;
        } else {
            BMR = (9.99 * mass) + (6.25 * height) - (4.92 * age) + 5;

        }
        BMR *= 100;
        BMR = Math.round(BMR);
        BMR /= 100;

        BMRresult.setText(Double.toString(BMR));
        BMRresult.setVisibility(View.VISIBLE);
        yourBMR.setVisibility(View.VISIBLE);

    }

    public void BMRbackButtonClicked(View view) {
        onPause();
        setContentView(R.layout.activity_start_screen);


    }




}
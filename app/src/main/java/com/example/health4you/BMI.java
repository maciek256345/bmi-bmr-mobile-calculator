package com.example.health4you;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;


public class BMI extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bmi);
    }


    public void BMICalculateButtonClicked(View view) {
        EditText edtMass = (EditText) findViewById(R.id.edtMass);
        EditText edtHeight = (EditText) findViewById(R.id.edtHeight);
        TextView BMIresult = (TextView) findViewById(R.id.tvBMIResult);
        TextView yourBMI = (TextView) findViewById(R.id.tvYourBMI);


        double mass = Double.parseDouble(edtMass.getText().toString());
        double height = Double.parseDouble(edtHeight.getText().toString());
        height /= 100;

        double BMI = mass / (height * height);
        BMI *= 100;
        BMI = Math.round(BMI);
        BMI /= 100;

        BMIresult.setText(Double.toString(BMI));
        BMIresult.setVisibility(View.VISIBLE);
        yourBMI.setVisibility(View.VISIBLE);

    }


}

